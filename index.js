// build server
var express = require('express');
var app = express();
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', './views');

var server = require('http').Server(app);
var io = require('socket.io')(server);
server.listen(3000, function () {
  console.log('Listen with port 3000');
});

// tạo kết nối giữa client và server
io.on('connection', function (socket) {
  console.log('connect', socket.id);
  socket.on('disconnect', function () {
    console.log('disconnect');
  });

  //server lắng nghe dữ liệu từ client
  socket.on('Client-sent-data', function (data) {
    //sau khi lắng nghe dữ liệu, server phát lại dữ liệu này đến các client khác
    console.log(data);
    io.emit('Server-sent-data', data);
  });

  //server lắng nghe dữ liệu từ noti
  socket.on('Client-sent-noti', function (data) {
    //sau khi lắng nghe dữ liệu, server phát lại dữ liệu này đến các client khác
    console.log(data);
    io.emit('Server-sent-noti', data);
  });
});

// create route, display view

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});
